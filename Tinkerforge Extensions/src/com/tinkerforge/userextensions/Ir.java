package com.tinkerforge.userextensions;
import com.tinkerforge.*;

import java.sql.Timestamp;
import java.util.Date;
public class Ir extends BrickletDistanceIR
{

	Timestamp timestamp;
	Date date;
	boolean active;
	
	public Ir(String uid, IPConnection ipcon)
	{
		super(uid, ipcon);
		// TODO Auto-generated constructor stub
	}

	public void setDate(Date date)
	{
		this.date = date;
	}
	
	public Date getDate()
	{
		return this.date;
	}
	
	public void setTimeStamp(Timestamp timestamp)
	{
		this.timestamp = timestamp;
	}
	
	public Timestamp getTimestamp()
	{
		return this.timestamp;
	}
	
	public void setActive(boolean active)
	{
		this.active = active;
	}

	public boolean getActive()
	{
		return this.active;
	}
}
