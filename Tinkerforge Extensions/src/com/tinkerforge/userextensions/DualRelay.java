package com.tinkerforge.userextensions;
import com.tinkerforge.*;


public class DualRelay extends BrickletDualRelay 
{

	public DualRelay(String uid, IPConnection ipcon)
	{
		super(uid, ipcon);
	}

	/*
	 * This method changes the state of the relay regardless of the current state.
	 * 
	 * @author Anton Gregersen.
	 * @param Int relay. 
	 * @return void.
	 * 
	 */
	public void switchState(int relay) //make overload for changing both relays at the same time.
	{
		try {

			boolean relay1 = getState().relay1;
			boolean relay2 = getState().relay2;

			if(relay == 0)
			{
				if (relay1)
				{
					setState(false, relay2);
				} else {
					setState(true, relay2);
				}
			} else if(relay == 1)
			{
				if(relay2)
				{
					setState(relay1, false);
				} else {
					setState(relay1, true);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}



	}
}
